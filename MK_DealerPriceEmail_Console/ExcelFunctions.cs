﻿/*
   Microsoft SQL Server Integration Services Script Task
   Write scripts using Microsoft Visual C# 2008.
   The ScriptMain is the entry point class of the script.
*/

using System;
using System.Drawing;
using System.Data;
using Excel = Microsoft.Office.Interop.Excel;
using System.Text.RegularExpressions;

namespace Kencove.Pricesheet.Dealer
{
    public class ExcelFunctions
    {
        private readonly object _om = Type.Missing;

        private Excel.Application _excelApp;
        public Excel.Application ExcelApp
        {
            get { return _excelApp = _excelApp ?? InstantiateExcelApplication(); }
        }
        
        private Excel.Application InstantiateExcelApplication()
        {
            return new Excel.Application
            {
                DisplayAlerts = false,
                Caption = Guid.NewGuid().ToString().ToUpper()
            };
        }

        public bool FillExcelWithDataset(String excelToFillPath, int startingRow, int startingColumn, DataTable data, String sheetToFillName = null)
        {
            Excel.Workbook excelToFillWb = ExcelApp.Workbooks.Open(excelToFillPath);
            if (excelToFillWb == null) return false;
            Excel.Worksheet excelToFillWs = String.IsNullOrEmpty(sheetToFillName) ? (Excel.Worksheet)excelToFillWb.ActiveSheet : excelToFillWb.Worksheets[sheetToFillName];

            excelToFillWs.Range[
                GetExcelColumnName(startingColumn) + (startingRow + 1)
                , GetExcelColumnName(startingColumn + data.Columns.Count-1) + ((startingRow + 1) + data.Rows.Count-1)
                ].Value2 = DataTableToMultiDimensionalArray(data);

            excelToFillWb.Close(true, excelToFillPath);
            return true;
        }

        private static string[,] DataTableToMultiDimensionalArray(DataTable data)
        {
            string[,] sa = new string[data.Rows.Count, data.Columns.Count];
            for (int r = 0; r < data.Rows.Count; r++)
                for (int c = 0; c < data.Columns.Count; c++)
                    sa[r, c] = data.Rows[r][c].ToString();
            return sa;
        }

        private string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber + 1;// for 0 index columnNumber
            string columnName = String.Empty;

            while (dividend > 0)
            {
                int modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo) + columnName;
                dividend = ((dividend - modulo) / 26);
            }

            return columnName;
        }

        public bool FormatHeaders(String targetPath, String headerPath)
        {
            Excel.Workbook headerWb = ExcelApp.Workbooks.Open(headerPath);
            Excel.Worksheet headerWs = headerWb.Worksheets["Header sheet"];
            //Add header to sheets
            Excel.Workbook targetWb = ExcelApp.Workbooks.Open(targetPath);
            foreach (Excel.Worksheet targetSheet in targetWb.Sheets)
            {
                MergeHeaders(targetSheet, headerWs);
                if (targetSheet.Name.Contains("Changed"))
                    targetSheet.Range["G2", "G2"].Value2 =
                        targetSheet.Range["G2", "G2"].Value2.ToString().Replace("Product", "Changed");
            }
            targetWb.Close(true, targetPath);
            headerWb.Close(false);

            return true;
        }

        public bool MergeWorksheets(String targetPath, String additionalPath)
        {
            Excel.Workbook targetWb = ExcelApp.Workbooks.Open(targetPath);
            Excel.Workbook additionalWb = ExcelApp.Workbooks.Open(additionalPath);
            foreach (Excel.Worksheet additionalSheet in additionalWb.Sheets)
            {
                additionalSheet.Copy(Type.Missing,targetWb.Worksheets[targetWb.Worksheets.Count]);
            }
            targetWb.Close(true, targetPath);
            additionalWb.Close(false);
            return true;
        }

        public bool FormatRows(String targetPath, int rows, int headerHeight, String sheetToFillName = null)
        {
            Excel.Workbook targetWb = ExcelApp.Workbooks.Open(targetPath);
            foreach (Excel.Worksheet targetSheet in targetWb.Sheets)
            {
                if (targetSheet.Name == sheetToFillName || sheetToFillName == null)
                    FormatRow(targetSheet, rows, headerHeight);
            }
            targetWb.Close(true, targetPath);
            return true;
        }

        public bool ActivateWorksheet(String targetPath, int sheetIndex)
        {
            Excel.Workbook targetWb = ExcelApp.Workbooks.Open(targetPath);
            if (sheetIndex < 1) sheetIndex = 1;
            if (sheetIndex > targetWb.Worksheets.Count) sheetIndex = targetWb.Worksheets.Count;
            targetWb.Worksheets.Add(targetWb.Worksheets[sheetIndex]);
            targetWb.Worksheets[sheetIndex].Delete();
            targetWb.Close(true, targetPath);
            return true;
        }

        private static void MergeHeaders(Excel.Worksheet actingWs, Excel.Worksheet headerWs)
        {
            Excel.Range headerPtr = headerWs.Range["A1", "O3"];
            Excel.Range actingPtr = actingWs.Range["A1", "O1"];
            for (int i = 0; i < 3; i++)
            {
                actingPtr.EntireRow.Insert(Excel.XlInsertShiftDirection.xlShiftDown, true);
                actingPtr = actingWs.Range["A1", "O1"];
            }
            actingPtr = actingWs.Range["A1", "O3"];
            headerPtr.Copy(actingPtr);

            actingPtr = actingWs.Range["O3", "O3"];
            actingPtr.Value2 = actingPtr.Value2.ToString().Replace(
                "[DATE]", DateTime.Now.AddDays(-1).ToString("MM/dd/yy"));

            actingWs.Cells.Borders.LineStyle = Excel.XlLineStyle.xlLineStyleNone;
        }
        
        private void SortWorksheet(Excel.Worksheet ws, int rows)
        {
            Excel.Range rangePointer = ws.Range["A5", "O" + (rows + 4)];
            rangePointer.Sort(
                ws.Range["A5", "A" + (rows + 4)], Excel.XlSortOrder.xlAscending,
                ws.Range["B5", "B" + (rows + 4)], _om, Excel.XlSortOrder.xlAscending,
                ws.Range["C5", "C" + (rows + 4)], Excel.XlSortOrder.xlAscending,
                Excel.XlYesNoGuess.xlYes, _om, false,
                Excel.XlSortOrientation.xlSortColumns);
        }

        private void FormatRow(Excel.Worksheet ws, int rows, int headerHeight)
        {
            SortWorksheet(ws, rows);
            Excel.Range rangePointer;

            for(char c = 'F'; c <= 'O'; c++)
            {
                ws.Range[c.ToString() + (headerHeight + 1), c.ToString() + (rows + headerHeight)].Value = ws.Range[c.ToString() + (headerHeight + 1), c.ToString() + (rows + headerHeight)].Value;
            }
            for (int i = headerHeight + 1; i <= rows + headerHeight; i++)
            {
                rangePointer = ws.Range["A" + i, "O" + i];
                rangePointer.Interior.Color = ColorTranslator.ToWin32(i % 2 == 1 ? Color.FromArgb(229, 224, 236) : Color.White);
            }

            //Add Grid Lines
            rangePointer = ws.Range["A" + (headerHeight + 1), "O" + (rows + headerHeight)];
            rangePointer.Borders.Item[Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Excel.XlLineStyle.xlLineStyleNone;
            rangePointer.Borders.Item[Excel.XlBordersIndex.xlInsideVertical].LineStyle = Excel.XlLineStyle.xlContinuous;
            rangePointer.Borders.Weight = Excel.XlBorderWeight.xlThin;
        }
    }
}