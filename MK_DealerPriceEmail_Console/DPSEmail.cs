﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Kencove.Pricesheet.Dealer
{
    public class DpsEmail
    {
        private List<String> _changedItems;
        public List<String> ChangedItems{ get { return _changedItems= _changedItems??new List<String>(); } }
        public string Name, Freq, Email, CustomerNo, PriceLevel, Email2;
        public int PriceChanges;
        public DateTime? LastSent;
        public DateTime SendAfter;
        public Boolean Inactive, Fax, AttachPdf, AttachSpreadsheet, ContractPricing;

        public String PricesFinal
        {
            get { return String.Format(ConfigurationManager.AppSettings["FINAL_Prices"], DateTime.Now.ToString("yyyy-MM-dd"), CustomerNo); }
        }

        public String Body
        {
            get
            {
                return "Dear " + Name + ",<br><br>Attached is an updated Kencove Dealer Price Sheet.  "
                       + (!(Inactive) && (ChangedItems.Count > 0)
                           ? "<br>Please view items: " + String.Join(", ", ChangedItems.OrderBy(s => s)) + " For their new, updated pricing."
                           : String.Empty)
                       + "<br><br>"
                       + ConfigurationManager.AppSettings["MarketingMessage"]
                       + (AttachSpreadsheet
                           ? "If you are receiving the Excel format, please note that there are two tabs." +
                             "  The \"Product Price List\" tab lists the entire price list, and the \"Changed Prices\"" +
                             " tab lists price changes since your last mailing." +
                             "  Also note that the document is in Microsoft Excel 2007+ (.xlsx) format." +
                             "  If you have trouble viewing the file due to an older version of office," +
                             " please install the free Excel Compatability viewer here:<br>"
                             + new Uri("https://www.microsoft.com/en-us/download/details.aspx?id=3")
                             +"<br>If you do not have Excel, there are free alternatives such as LibreOffice available" +
                             " which can be downloaded from here:<br>"
                             + new Uri("https://www.libreoffice.org/download/libreoffice-fresh/")
                             + "<br><br>"
                           : String.Empty)
                       + "If you have any questions, please visit our website at: "
                       + new Uri("http://www.kencove.com")
                       + " or feel free to contact us by phone or email.<br><br>"
                       +"Thank you for your business," +
                       "<br><br>Kencove Farm Fence<br>1-800-536-2683<br>marketing@kencove.com";
            }
        }

        public String Attachments
        {
            get
            {
                String attachment = (Fax || AttachPdf
                    ? String.Format(ConfigurationManager.AppSettings["DealerAlpha"], DateTime.Now.ToString("yyyy-M-dd"))
                    : String.Empty);
                if (Fax || !AttachSpreadsheet) return attachment;
                if (attachment.Length > 0) attachment += "|";
                attachment += PricesFinal;
                return attachment;
            }
        }
    }
}
