﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Kencove.Connections;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System.Net;

namespace Kencove.Pricesheet.Dealer
{
    internal class Program
    {

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool EndTask(IntPtr hWnd, bool fShutDown, bool fForce);

        [DllImport("user32.dll", SetLastError = false)]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        [DllImport("user32.dll", SetLastError = false)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        private static Stopwatch _appSw;
        private static Stopwatch AppSw
        {
            get{return _appSw = (_appSw ?? ((Func<Stopwatch>) (() =>{Stopwatch s = new Stopwatch();s.Start();return s;})).Invoke());}
        }

        private static void WriteConsoleMessage(String message)
        {
            Console.WriteLine(AppSw.Elapsed.TotalMilliseconds + "ms\t " + message);
        }

        private static void Main(string[] args)
        {
            ExcelFunctions excelFunctions = null;
            try
            {
                //SendEmail(to: "thill@kencove.com", subject: "Test", body: "DPE", mandrillRecipientName: "TONY TEST", mandrillFromEmail: "kencoveit@kencove.com", mandrillFromName: "Kencove TEST");
                //return;
                WriteConsoleMessage("Starting Kencove.Pricesheet.Dealer.Program");
                WriteConsoleMessage("Killing all open Excel instances");
                KillAllExcels();

                ////Generate PDF
                WriteConsoleMessage("Creating dealerAlpha.pdf from SSRS");
                CreateDealerPriceSheetPdf();
                WriteConsoleMessage("Done!");

                ////Check for Prices that need Approved
                WriteConsoleMessage("Checking that all price changes have been Approved");
                if (CheckPricesApproved() > 0)
                {
                    WriteConsoleMessage("Price changes exist, emailing dpe@kencove.com");
                    SendEmail(to: "dpe@kencove.com",
                        subject: "Prices for Review Exist",
                        body: CheckPricesApproved() + " prices have been entered and not reviewed. Please approve them or make the corrections nessesary to address any issues found. Link To Report: https://csi-db.ad.kencove.com/reports/report/SL_Kencove/IC_KenItemPriceReview ",
                        profileName: "Exchange");
                    WriteConsoleMessage("Early Exit");
                    return;
                };
                WriteConsoleMessage("Done!");

                ////Check for Missing ItemGroups
                WriteConsoleMessage("Checking for missing Item Groups");
                //if (CheckForNullIg() > 0)
                //{
                //    WriteConsoleMessage("Missing Item Groups exist, emailing dpe@kencove.com");
                //    SendEmail(to: "dpe@kencove.com",
                //        subject: "Items Missing ItemGroups",
                //        body: CheckForNullIg() + " Items are missing their item groups.  Please view report at http://kencoveinfo/reporting/ReportsLibrary/Marketing/MK_ItemGroupMissing.rdl .",
                //        profileName: "Exchange");
                //    WriteConsoleMessage("Early Exit");
                //    return;
                //};
                WriteConsoleMessage("Done!");

                ////Process Mail Labels (TODO Async)
                WriteConsoleMessage("Processing Mail Labels");
                ProcessMailLabels();
                WriteConsoleMessage("Done!");

                WriteConsoleMessage("Initializing Excel Functions");
                excelFunctions = new ExcelFunctions();
                WriteConsoleMessage("Done!");

                //Get Emails
                WriteConsoleMessage("Getting Dealer Email List");
                List<DpsEmail> EmailList = GetEmails();
                WriteConsoleMessage("Done!");

                //Get Message
                ConfigurationManager.AppSettings["MarketingMessage"] = GetMarketingMessage();
                if (ConfigurationManager.AppSettings["MarketingMessage"].Length > 0)
                    ConfigurationManager.AppSettings["MarketingMessage"] += "\r\n\r\n";

                //Copy template
                WriteConsoleMessage("Making Working copy of template");
                File.Copy(ConfigurationManager.AppSettings["BASE_Format"],
                    ConfigurationManager.AppSettings["WORKING_Load"], true);
                WriteConsoleMessage("Done!");

                //Add Header to top
                WriteConsoleMessage("Merging header to top of document");
                excelFunctions.FormatHeaders(ConfigurationManager.AppSettings["WORKING_Load"],
                    ConfigurationManager.AppSettings["BASE_Header"]);
                WriteConsoleMessage("Done!");

                //Export prices into Template
                WriteConsoleMessage("Filling complete price list in Working copy");
                using (DataSet dealerPrices = GetDealerPrices())
                { 
                    if (dealerPrices.Tables.Count < 1) return;
                    int prices=dealerPrices.Tables[0].Rows.Count;
                    excelFunctions.FillExcelWithDataset(ConfigurationManager.AppSettings["WORKING_Load"], 4, 0,
                        dealerPrices.Tables[0], "Product Price List");
                    excelFunctions.FormatRows(ConfigurationManager.AppSettings["WORKING_Load"], prices, 4, "Product Price List");
                }
                WriteConsoleMessage("Done!");
                
                //Process List and create excels
                WriteConsoleMessage("Starting: Processing dealer list");
                //foreach (var email in EmailList.Where(e => e.CustomerNo == "0006638"))
                //foreach (var email in EmailList.TakeRandom(10))
                foreach (var email in EmailList)
                {
                    WriteConsoleMessage(email.CustomerNo + ": " + email.Name);
                    if (email.AttachSpreadsheet)
                        File.Copy(ConfigurationManager.AppSettings["WORKING_Load"], email.PricesFinal, true);

                    if (email.LastSent.HasValue && email.AttachSpreadsheet)
                        using (DataSet dealerPriceChanges = GetDealerPriceChanges(email.LastSent.Value))
                            if (dealerPriceChanges.Tables.Count >= 1 && dealerPriceChanges.Tables[0].Rows.Count > 0)
                            {
                                WriteConsoleMessage("Loading changes...");
                                int changes = dealerPriceChanges.Tables[0].Rows.Count;
                                if (changes < 20)
                                    email.ChangedItems.AddRange(dealerPriceChanges.Tables[0].AsEnumerable().Select(r => r["ItemCode"].ToString()));
                                excelFunctions.FillExcelWithDataset(email.PricesFinal, 4, 0, dealerPriceChanges.Tables[0], "Changed Prices");
                                excelFunctions.FormatRows(email.PricesFinal, changes, 4, "Changed Prices");
                                WriteConsoleMessage(changes + " changes.");
                            }
                    if (email.AttachSpreadsheet && email.ContractPricing)
                        using (DataSet dealerPriceContract = GetDealerPriceContract(email.CustomerNo))
                            if (dealerPriceContract.Tables.Count >= 1 && dealerPriceContract.Tables[0].Rows.Count > 0)
                            {
                                WriteConsoleMessage("Loading contract...");
                                excelFunctions.MergeWorksheets(email.PricesFinal, ConfigurationManager.AppSettings["CONTRACT_Sheet"]);
                                int contract = dealerPriceContract.Tables[0].Rows.Count;
                                excelFunctions.FillExcelWithDataset(email.PricesFinal, 4, 0, dealerPriceContract.Tables[0], "Contract Price List");
                                excelFunctions.FormatRows(email.PricesFinal, contract, 4, "Contract Price List");

                                WriteConsoleMessage(contract + " contract prices.");
                            }

                    excelFunctions.ActivateWorksheet(email.PricesFinal, 0);

                    WriteConsoleMessage("Logging send to " + email.CustomerNo + " for future change point.");
                    LogPricesheetEmailed(email.CustomerNo);

                    WriteConsoleMessage("Emailing " + email.Email);
                    SendEmail(to: email.Email,
                            bcc: "web_emails@kencove.com",
                            subject: "Your Kencove price Sheet",
                            body: email.Body,
                            attachments: email.Attachments,
                            type:"DE",
                            docKey:email.CustomerNo,
                            mandrillRecipientName: email.Name,
                            mandrillFromEmail: "marketing@kencove.com",
                            mandrillFromName: "Kencove Farm Fence Supplies");
                }

                WriteConsoleMessage("Done!");

                Console.WriteLine("");
            }
            catch (Exception e)
            {
                Console.Write(e);
                SendEmail(to: "kencoveit@kencove.com",
                        subject: "Dealer Price Sheet Exception " + DateTime.Now.ToString("yyyy-MM-dd"),
                        body: e.ToString(),
                        profileName: "Exchange");
            }
            finally
            {
                if (excelFunctions != null)
                {
                    excelFunctions.ExcelApp.Quit();
                    EnsureProcessKilled(excelFunctions.ExcelApp);
                }
            }
        }

        public static void CreateDealerPriceSheetPdf()
        {
            String coverPath=  String.Format(ConfigurationManager.AppSettings["DealerAlphaCoversheet"], DateTime.Now.ToString("yyyy-M-dd"));
            //= "//lh_files/common/Reports/DealerPriceSheet/" + DateTime.Now.ToString("yyyy-M-dd") + "/SO_dealerAlpha.pdf";
            Console.WriteLine(coverPath);
            //ConfigurationManager.AppSettings["DealerAlphaCoversheet"].ToString();//String.Format(ConfigurationManager.AppSettings["DealerAlphaCoversheet"],DateTime.Now.ToString("yyyy-M-dd"));
            String bodyPath = String.Format(ConfigurationManager.AppSettings["DealerAlphaBody"], DateTime.Now.ToString("yyyy-M-dd"));
            String alphaPath = String.Format(ConfigurationManager.AppSettings["DealerAlpha"], DateTime.Now.ToString("yyyy-M-dd"));
            //CreateSSRSReport(@"/Operations/MK_DealerPriceSheetCover",coverPath);
            //CreateSSRSReport(@"/Operations/MK_DealerPriceSheet_SSRS",bodyPath);
            ProcessPDF("ntlm", coverPath, "https://csi-db.ad.kencove.com/ReportServer?/SL_Kencove/Marketing/MK_DealerPriceSheetCover&rs:Format=PDF");
            ProcessPDF("ntlm", bodyPath, "https://csi-db.ad.kencove.com/ReportServer?/SL_Kencove/Marketing/MK_DealerPriceSheet_SSRS&rs:Format=PDF");
            MergePdf(coverPath, bodyPath, alphaPath, false);
        }

        public static int CheckPricesApproved()
        {
            return KencoveSqlConn.MethodSingleIntProc("MK_LogPrices_ToDo_Console");
        }

        public static int CheckForNullIg()
        {
            return KencoveSqlConn.MethodSingleIntProc("MK_DealerPriceSheet_NullIG");
        }

        public static void UpdateInactiveDealers()
        {
            using (KencoveSqlConn sqlConn = new KencoveSqlConn("KencoveMarketing", "CSI-DB"))
                sqlConn.ExecuteProc("MK_MarkInactiveDealers");
        }
        
        public static void ProcessMailLabels()
        {
            using (KencoveSqlConn sqlConn = new KencoveSqlConn("KencoveMarketing", "CSI-DB"))
                sqlConn.ExecuteProc("MK_DealerPriceSheet_LogMail");
        }
        public static String GetMarketingMessage()
        {
            using (KencoveSqlConn sqlConn = new KencoveSqlConn("KencoveMarketing", "CSI-DB"))
                return sqlConn.ExecuteOutProc("MK_DealerPriceSheet_Message", new String[] { }, new[] { "@message" })[0];
        }

        public static DataSet GetDealerPrices()
        {
            using (KencoveSqlConn sqlConn = new KencoveSqlConn("KencoveMarketing", "CSI-DB"))
                return sqlConn.ExecuteQuery("SELECT * FROM MK_DealerPriceSheet_View (NOLOCK)");
        }

        public static DataSet GetDealerPriceChanges(DateTime fromDate)
        {
            using (KencoveSqlConn sqlConn = new KencoveSqlConn("KencoveMarketing", "CSI-DB"))
                return sqlConn.ExecuteProc("MK_DealerPriceSheet_Changed", new[] { "@FromDate="+fromDate });
        }

        public static DataSet GetDealerPriceContract(String customerNo)
        {
            using (KencoveSqlConn sqlConn = new KencoveSqlConn("KencoveMarketing", "CSI-DB"))
                return sqlConn.ExecuteProc("MK_DealerPriceSheet_Contract", new[] { "@CustomerNo=" + customerNo });
        }

        public static void LogPricesheetEmailed(String customerNo)
        {
            using (KencoveSqlConn sqlConn = new KencoveSqlConn("KencoveMarketing", "CSI-DB"))
                sqlConn.ExecuteProc("MK_DealerPriceSheet_LogSingle", new[] { "@CustomerNo=" + customerNo });
        }


        public static void SendEmail(String to, String cc = "", String bcc = "", String subject = "", String body = "",
            String attachments = "",String type = "", String docKey = "", bool getApproval = true, String profileName = "Kencove",
            String mandrillRecipientName = "", String mandrillFromEmail = "",String mandrillFromName = "")
        {
            List<String> inputs = new List<string>();
            if (!String.IsNullOrEmpty(to))
                inputs.Add("@recipients=" + to);
            if (!String.IsNullOrEmpty(profileName))
                inputs.Add("@profile_name=" + profileName);
            if (!String.IsNullOrEmpty(cc))
                inputs.Add("@copy_recipients=" + cc);
            if (!String.IsNullOrEmpty(bcc))
                inputs.Add("@blind_copy_recipients=" + bcc);
            if (!String.IsNullOrEmpty(subject))
                inputs.Add("@subject=" + subject);
            if (!String.IsNullOrEmpty(body))
                inputs.Add("@body=" + body);
            if (!String.IsNullOrEmpty(attachments))
                inputs.Add("@file_attachments=" + attachments);
            if (!String.IsNullOrEmpty(type))
                inputs.Add("@type=" + type);
            if (!String.IsNullOrEmpty(docKey))
                inputs.Add("@docKey=" + docKey);
            if (!getApproval)
                inputs.Add("@getApproval=" + 1); // set to require approval for now
            if (!String.IsNullOrEmpty(mandrillRecipientName))
                inputs.Add("@MandrillRecipientNames=" + mandrillRecipientName);
            if (!String.IsNullOrEmpty(mandrillFromEmail))
                inputs.Add("@MandrillFromEmail=" + mandrillFromEmail);
            if (!String.IsNullOrEmpty(mandrillFromName))
                inputs.Add("@MandrillFromName=" + mandrillFromName);

            using (KencoveSqlConn sqlConn = new KencoveSqlConn("KencoveCSI", "CSI-DB"))
                sqlConn.ExecuteProc("KC_Email_Queue", inputs.ToArray());
        }

        public static List<DpsEmail> GetEmails()
        {
            List<DpsEmail> list = new List<DpsEmail>();
            
            using (KencoveSqlConn sqlConn = new KencoveSqlConn("KencoveMarketing", "CSI-DB"))
            using (DataSet ds = sqlConn.ExecuteProc("MK_DealerPriceSheet_Log", new[] { "@log=0", "@count=0" }))
            {
                if (ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                    return list;
                list.AddRange(from DataRow row in ds.Tables[0].Rows
                    select new DpsEmail
                    {
                        Name = row["Name"].ToString(), 
                        Freq = row["Freq"].ToString(), 
                        Email = row["Email"].ToString(), 
                        CustomerNo = row["CustomerNo"].ToString(), 
                        PriceLevel = row["PriceLevel"].ToString(),
                        AttachSpreadsheet = row["AttachSpreadsheet"].ToString() == "1",
                        AttachPdf = row["AttachPDF"].ToString() == "1",
                        Fax = row["Fax"].ToString() == "1",
                        LastSent = row["LastSent"] == DBNull.Value ? (DateTime?)null : DateTime.Parse(row["LastSent"].ToString()), 
                        PriceChanges = int.Parse(row["PriceChanges"].ToString()), 
                        SendAfter = DateTime.Parse(row["SendAfter"].ToString()),
                        Email2 = row["Email2"].ToString(),
                        Inactive = row["Inactive"].ToString() == "1",
                        ContractPricing = row["ContractPricing"].ToString() == "1"
                    });
            }

            return list;
        }
        
        public static int GetPriceRowCount()
        {
            return KencoveSqlConn.MethodSingleIntProc("MK_DealerPriceSheet_COUNT");
        }
        public static void ProcessPDF(String credentials, String filePath, String uriString)
        {
            //download from url
            using (WebClient webClient = new WebClient())
            {
                try
                {
                    Uri uri = new Uri(uriString);
                    //using credentials
                    if (!String.IsNullOrEmpty(credentials) && credentials == "ntlm")
                    {
                        webClient.UseDefaultCredentials = false;
                        webClient.Credentials = new NetworkCredential("ssrs_client", "yYyaWnoGf0f", "kencovepa");
                        webClient.Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
                        // webClient.Credentials = windowsCredential;
                    }

                    try
                    {
                        webClient.DownloadFile(uri, filePath);
                    }
                    catch (Exception exc)
                    {
                        //webClient.DownloadFile(uri, @"C:\Users\wlin\Newfolder\New folder\Test\DealerPriceSheetCover.pdf");
                        Console.WriteLine("failed");
                    }

                }
                catch (Exception exc)
                {
                    //email exception

                    Console.Out.WriteLine("Not Ok");
                }
            }
        }
        public static void CreateSSRSReport(string reportName, string pdfName)
        {
            RE2005.ReportExecutionService rsExec = new RE2005.ReportExecutionService
            {
                Credentials = System.Net.CredentialCache.DefaultCredentials,
                Url = "https://csi-db.ad.kencove.com/reports/reportexecution2005.asmx"
                // "http://sqlent/reportserver/reportexecution2005.asmx"
            };

            string fileName = pdfName; 

            try
            {
                rsExec.LoadReport(reportName, null);
                string encoding;
                string extension;
                string mimeType;
                RE2005.Warning[] warnings;
                string[] streamIDs;

                Byte[] results = rsExec.Render("PDF", null, out extension, out encoding, out mimeType, out warnings, out streamIDs);

                using (FileStream stream = File.OpenWrite(fileName))
                {
                    stream.Write(results, 0, results.Length);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void MergePdf(string frontPdfPath, string backPdfPath, string mergedPdfPath, bool keep)
        {
            using (PdfDocument frontPdf = PdfReader.Open(frontPdfPath, PdfDocumentOpenMode.Import))
            using (PdfDocument backPdf = PdfReader.Open(backPdfPath, PdfDocumentOpenMode.Import))
            using (PdfDocument mergedPdf = new PdfDocument())
            {
                CopyPages(frontPdf, mergedPdf);
                CopyPages(backPdf, mergedPdf);
                mergedPdf.Save(mergedPdfPath);
            }
            if (keep) return;

        }

        public static void CopyPages(PdfDocument from, PdfDocument to)
        {
            for (int i = 0; i < from.PageCount; i++)
                to.AddPage(from.Pages[i]);
        }

        private static void EnsureProcessKilled(Microsoft.Office.Interop.Excel.Application exApp)
        {
            uint iRes, iProcID;

            //grab the handle
            IntPtr MainWindowHandle = new IntPtr(exApp.Parent.Hwnd);

            //if for some reason exApp loses the handle, 
            //  run a search to see if the process is still running
            if (MainWindowHandle == IntPtr.Zero)
                MainWindowHandle = FindWindow("", exApp.Caption);

            //if the process is already dead, then leave function
            if (MainWindowHandle == IntPtr.Zero)
                return;

            //find the process ID by the handle
            iRes = GetWindowThreadProcessId(MainWindowHandle, out iProcID);
            if (iProcID == 0)
            {
                //if the ProcessorArchitecture id failed, most likely an error
                if (EndTask(MainWindowHandle, false, true) == false) return;
                else throw new ApplicationException("Failed to close.");
            }
            System.Diagnostics.Process proc = System.Diagnostics.Process.GetProcessById((int)iProcID);

            //closes the main window and refreshes
            proc.CloseMainWindow();
            proc.Refresh();

            //returns if that stops the process
            if (proc.HasExited) return;
            //force kills the process when all else fails
            proc.Kill();
        }

        public static void KillAllExcels()
        {
            foreach (System.Diagnostics.Process proc in System.Diagnostics.Process.GetProcessesByName("EXCEL"))
                proc.Kill();
        }
    }

    public static class MyExtentions
    {
        public static List<T> TakeRandom<T>(this List<T> list, int take)
        {
            Random r = new Random();
            List<T> rList = new List<T>();
            for(int i = list.Count; i>0 && take>0; i--)
            {
                if (!(r.Next(10000) < ((float)take/i)*10000)) continue;
                rList.Add(list[i-1]);
                take--;
            }
            return rList;
        }
    }
}
